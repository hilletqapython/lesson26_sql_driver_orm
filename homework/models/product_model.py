from sqlalchemy import Column, Integer, VARCHAR
from sqlalchemy.orm import declarative_base

Base = declarative_base()


class Product(Base):
    """
    Class that describe 'product' table in 'store' database
    """
    __tablename__ = 'product'

    id: int = Column(Integer, primary_key=True)
    name: str = Column(VARCHAR(255))
    price: int = Column(Integer)

    def __str__(self):
        """
           Returns:
               (str) table item
        """
        return f"id: {self.id} | name: {self.name} | price: {self.price}"

