from typing import Optional, Type

from sqlalchemy.orm import Session

from lesson26.homework.models.product_model import Product
from lesson26.homework.session import session


class ProductRepository:
    """
        Class contain methods that works with 'product' table items
    """
    def __init__(self, session: Session):
        self.session: Session = session

    def get_all(self):
        for each_product in self.session.query(Product).all():
            print(each_product)

    def get_by_id(self, product_id: int) -> Optional[Type[Product]]:
        """
        Args:
            product_id:(int) product id from 'product' table
        Returns:
            (Product) product row from table
        """
        return self.session.get(Product, {'id': product_id})

    def update_by_id(self, product_id: int, data: dict):
        """
        Args:
            product_id: (int) product id from 'product' table
            data: (dict) information that we want to update
        """
        self.session.query(Product).filter_by(id=f"{product_id}").update(data)
        self.session.commit()

    def add_new_product(self, product: Product):
        """
        Args:
            product: (Product) new Product class element with name and price attributes
        """
        self.session.add(product)
        self.session.commit()

    def delete_product(self, product_id: int):
        """
            Args:
                product_id: (int) product id from 'product' table
        """
        self.session.query(Product).filter_by(id=f"{product_id}").delete()
        self.session.commit()


if __name__ == "__main__":
    product = ProductRepository(session)
    product.get_all()
    # print(product.get_by_id(4))
    # product.update_by_id(4, {'name': 'fish', 'price': 5})
    # product.add_new_product(Product(name='sour cream', price=1))
    # product.get_all()
    # product.delete_product(2)
    product.get_all()
