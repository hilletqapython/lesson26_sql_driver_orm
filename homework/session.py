from sqlalchemy import create_engine
from sqlalchemy.orm import Session, sessionmaker

engine = create_engine("postgresql://postgres:postgres@localhost/store")
__session = sessionmaker(engine)
session: Session = __session()
